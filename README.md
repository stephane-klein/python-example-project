# Python Example project

## Prerequisite

- [Python version 3](https://www.python.org/)
- [Git](https://git-scm.com/)

On OSX, install Python and Git with Homebrew package manager ([how install it](https://brew.sh))

```
$ brew install python git
```

## Install Virtualenv and Python dependencies

```
$ git clone git@gitlab.com:stephane-klein/python-example-project.git
$ python3 -m venv .venv/
$ source .venv/bin/activate
$ pip install -r requirements.txt
```

## Execute program

Execute `main.py` program:

```
$ python main.py
```